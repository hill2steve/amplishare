#!/usr/bin/python
#===============================================================================
# TODO: So we know that the threaded filetransfer on one file fails miserably (can't share handlers) 
#    What we need to do is break the file up into n many parts, create a unique header for that file (idk like &nthread&filepart&filename)
#    When downloading, it needs to list all the files (everytime unfortunately), look for our filename with the header (or trailer idk), and if it's
#    our file we need to use a custom handler for writing to the disk. This also means we need to change our list function so that it ommits files that have the special header.
#
# TODO: Finish the HTMLParse for the list command (files vs directories)
# 
# TODO: Create an authentication class. This wraps all our requests with Authentication
#
# TODO: Put all our shit into a class instead of random functions and globals.
#===============================================================================
import os
import pycurl
import sys
import json
import random
import Queue
import threading
import time
from argparse import ArgumentParser 
from argparse import ArgumentError
from math import floor
import errno
from random import Random
from HTMLParser import HTMLParser
#main function

def main():
    #send_object_chunked("testfile", "128.193.84.20:8080/namespace/test/landing")
    #fd = open('../../.response', 'r')
    #parser = ListHTMLParser(fd)
    #print parser.parse_lines_by("<tr>")
    sender = AmpliShare()

class AmpliShare:
    #Not all of these are being used. Cleanup.
    current = None
    INCREMENT = None
    bytes_read = None
    BLOCKSIZE = None
    file_size = None
    num_threads = None
    start_time = None
    end_time = None
    bytes_transferred = None

    #Our Queues. They are worked on by the threads.
    todo = Queue.Queue()
    directories = Queue.Queue()
    statistics = Queue.Queue()

    #Menu handles the command line arguments and the actions associated with them.
    #Create some md5 hash method to ensure files stored are the same as the files retrieved.
    #Kind of a receipt...
    def __init__(self):
        #Instance variable declarations
        self.num_threads = 1
        self.current = 0
        self.INCREMENT = 5
        self.bytes_read = 0
        self.BLOCKSIZE = 8
        self.file_size = 0
        self.start_time = time.time()
        self.end_time = time.time()
        self.bytes_transferred = 0
    
        #Our Queues. They are worked on by the threads.
        self.todo = Queue.Queue()
        self.directories = Queue.Queue()
        self.statistics = Queue.Queue()
        
        #Builds our menu.
        parser = ArgumentParser(description='Please input what you would like to do.')
        parser.add_argument('-p', '--put', action='append', nargs='+', 
                    help="A put request to amplidata. Put arg1(file path) into arg2(amplidata namespace). Defaults to never overwrite already existing files. Use -f to override this.", 
                    metavar=('localfiles'))
        parser.add_argument('-g', '--get', action='append', nargs=1, 
                    help="A get request to amplidata. Gets the file at namespace arg1 into location arg2", 
                    metavar=('remote_namespace'))
        parser.add_argument('-d', '--delete', action='append_const', const=True, 
                    help="A delete request to amplidata. Deletes the object at arg1 from the objectstore", 
                    metavar=('namespace'))
        parser.add_argument('-ls', '--list', action='append_const', const=True, 
                    help="Lists the objects in the given namespace. The namespace is a part of the url.", 
                    metavar=('namespace'))
        parser.add_argument('-m', '--mkdir', action='append', nargs=1, 
                    help="Creates a directory named arg2 in the namespace of arg1.", 
                    metavar=('namespace'))
        #our menu should always have a url, url is required. includes the new file location and the files location
        parser.add_argument('-u', '--url', action='append', nargs=1, required=True, 
                    help="The URL that is sent through cURL. This includes the directory structure that we are downloading/uploading from. In the case of uploading, the name of the file that we place on the server should be the end of the argument.",
                    metavar=('url'))
        parser.add_argument('-t','--threads', action='append', nargs=1,
                        help="The number of threads to use when transferring files (one per file)",
                        metavar=('n-threads'))
        parser.add_argument('-f', '--force', action='append_const', const=True,
                        help="Overwrites any objects with a matching name during transfer.",
                        )
        
        #Parses the user input.
        try:
            args = parser.parse_args()
        except Exception as error:
            print "Error: You must include the -u argument to your query. We have to know who the host is!"
            return
        #used to print usage
        arg_size = 0
        if args.threads != None:
            self.num_threads = int(args.threads[0][0])
        
        if args.get != None:
            try:
                download_object(args.url[0][0], args.get[0][0])
                arg_size += 1
            except pycurl.error:
                print "Invalid hostname or hostname lookup failed: " + args.url[0][0]
    
        if args.put != None:
            arg_size +=1
            try:
                for things in args.put[0]:
                    #if you put something, check what it is first.
                    if os.path.isdir(things):
                        self.directories.put(things)
                    else:
                        self.todo.put(things)
                    #if we have a directory, find all files in that directory tree and send them.
                    for root, files, dirs in os.walk(things):
                        for name in files:
                            if os.path.isdir(os.path.join(root,name)):
                                self.directories.put(os.path.join(root,name))
                            else:
                                self.todo.put(os.path.join(root,name))
                        for name in dirs:
                            if os.path.isdir(os.path.join(root,name)):
                                self.directories.put(os.path.join(root,name))
                            else:
                                self.todo.put(os.path.join(root,name))
                            
                while self.directories.empty() == False:
                    cur = self.directories.get()
                    self.create_directory(args.url[0][0], '/' + cur)
                    print "create dir:", cur
                #For each thread, assign it a task. (default 1 thread)
                for i in range(self.num_threads):
                    if args.force != None:
                        t = threading.Thread(target=self.upload_thread, args=[args.url[0][0], True])
                    else: 
                        t = threading.Thread(target=self.upload_thread, args=[args.url[0][0]])
                #python handles cleanup when program exits
                    t.daemon = True
                    t.start()
                    print "spawn thread:", i
                self.todo.join() #block until finished
                print "Finished"
                self.end_time = time.time()
                self.generate_statistics()
              
            except pycurl.error:
                print "Invalid hostname or hostname lookup failed: " + args.url[0][0] + '/'
    
        if args.delete != None:
            try:
                self.delete_object(args.url[0][0])
                arg_size += 1
            except pycurl.error:
                print "Invalid hostname or hostname lookup failed: " + args.url[0][0] 
            
        if args.list != None:
            try:
                self.list_objects(args.url[0][0])
                arg_size += 1
            except pycurl.error:
                print "Invalid hostname or hostname lookup " + args.url[0][0]
            
        if args.mkdir != None:
            try:
                self.create_directory(args.url[0][0], args.mkdir[0][0])
                arg_size += 1
            except pycurl.error:
                print "Invalid hostname or hostname lookup failed: " + args.url[0][0]      
             
        if (arg_size == 0):
            parser.print_usage()

    #Ask for confirmation first
    #Also permissions need to be setup "somehow"  

    #===============================================================================
    # delete_object deletes the object given at the extension of url as according to the amplidata api 
    # 
    # It then prints the response code. Although I may change this to return a string of "what happened"
    # 
    # Example:
    #     delete_object("amplidata/dir1/dir2/object")
    #===============================================================================
    def delete_object(self, url):
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.CUSTOMREQUEST, "DELETE")
        c.perform()
        c.close()
        # Probably should put information in a file that is easily readable, maybe csv
        print "Status Code: ", c.getinfo(pycurl.HTTP_CODE)    
        c.close()
        

    #===============================================================================
    # list_objects lists the objects at the namespace passed with url as according to the amplidata API 
    #     list_object expects the url to end in '/', if it does not, it prints a warning and adds the 
    #     forwardslash
    # It then prints the response code. Although I may change this to return a string of "what happened"
    # 
    # TODO: This function needs to be run through our HTML parser.
    #
    # Example:
    #     list_objects("amplidata/dir1/dir2/")
    #===============================================================================
    def list_objects(self, url):
        c = pycurl.Curl()
        headers = []
        headers.append("Accept: text/html")
        headers.append("Content-Type: binary/octet-stream")
        b = open('.response', 'a')
        c.setopt(pycurl.URL, url)
        #c.setopt(pycurl.WRITEFUNCTION, body)
        c.setopt(pycurl.HTTPHEADER, headers)
        #c.setopt(pycurl.CUSTOMREQUEST, "MKCOL")
        #c.setopt(pycurl.WRITEHEADER, out_header)
        c.setopt(pycurl.WRITEFUNCTION, b.write)
        print "Contents of: ", url
        c.perform()
        print "Status Code: ", c.getinfo(pycurl.HTTP_CODE)    
        c.close()
        b.close()
        #parse response
    
    #===============================================================================
    # create_directory makes a directory of name at the     os.close(1)
    #    amplidata API. Again if a '/' is ommitted from the end of url it prints a warning and automatically
    #    appends '/'
    #     
    # It then prints the response code. Although I may change this to return a string of "what happened"
    # 
    # Example:
    #     create_directory("amplidata/dir1/", dir2)
    #===============================================================================

    def create_directory(self, url, name):
        c = pycurl.Curl()
        headers = []
        if name[0] != '/':
            print "You used the name: ", name, " Next time add a '/' to the front of it for directories."
            name = '/' + name
            url += name
            print url
        b = open('.response', 'a')
        headers.append("Content-Type: application/json")
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.HTTPHEADER, headers)
        c.setopt(pycurl.WRITEFUNCTION, b.write)
        c.setopt(pycurl.CUSTOMREQUEST, "MKCOL")
        c.perform()
        print "Status Code: ", c.getinfo(pycurl.HTTP_CODE)
        b.close()
        c.close()


    #===============================================================================
    # send_object sends the object in filename to the namespace given in the url. The placement
    #    of the file will be at the end of the url, like amplidata/dir1/filename If there is a problem 
    #    reading the file, an error message is displayed.
    #     
    # It then prints the response code. Although I may change this to return a string of "what happened"
    # 
    # Example:
    #     send_object("/home/me/myfile.txt", amplidata/dir1/dir2/myfile.txt)
    #===============================================================================
    def send_object(self, filename, url, overwrite):
        # needs to do error handling
        c = pycurl.Curl()
        try:
            file_size = os.path.getsize(filename)
            fd = open(filename, "rb")
        except OSError:
            print "Something went terribly wrong. ", filename
            print OSError
            return
        # make this more specific and put it in append mode
        out_header = open(".response", "a")
        b = open('.response', 'wa')
        headers = []
        headers.append("Accept: application/json")
        headers.append("Content-Type: binary/octet-stream")
        if overwrite == False:
            headers.append("If-None-Match: *")

        c.setopt(pycurl.NOPROGRESS, 0)
        c.setopt(pycurl.PROGRESSFUNCTION, self.up_progress)
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.HTTPHEADER, headers)
        c.setopt(pycurl.PUT, 1)
        c.setopt(pycurl.INFILE, fd)
        c.setopt(pycurl.INFILESIZE, file_size)
        c.setopt(pycurl.WRITEFUNCTION, out_header.write)
        c.perform()
        # need to lookup more about getting the response mostly useless
        # probably a callback or something.....
        # Probably should put information in a file that is easily readable, maybe csv
        print "Status Code: ", c.getinfo(pycurl.HTTP_CODE)
        print "Total Transfer Time: ", c.getinfo(pycurl.TOTAL_TIME), " seconds."
        time = c.getinfo(pycurl.TOTAL_TIME)
        status = c.getinfo(pycurl.HTTP_CODE)
        if status == 201 or status == 200:
            self.bytes_transferred += file_size
        c.close()
        b.close()
        fd.close()
        return filename + "," + str(file_size) + "," + str(time) + ","+str(status)

        #===============================================================================
        # Sends objects chunked, this is what i've been using to test the parallel sending.
        # Works by using scan right now to pick different parts in the file and send, but try doing this
        # in parallel is the next step. My guess is it wont work and we will need to treat the files on the
        # server as one huge "directory" file.
        #===============================================================================
    def send_object_chunked(self, filename, url):
         c = pycurl.Curl()
         try:
             #TODO: Why is this global?
             self.file_size
             file_size = os.path.getsize(filename)
             fd = open(filename, "rb")
         except OSError:
             print "Something went terribly wrong."
             print OSError.message
             return
 
         headers = []
         headers.append("Accept: application/json")
         headers.append("Content-Type: binary/octet-stream")
 
         #important stuff, actually writes our things, this should probably be somewhere else.
         print "building my threads"
         c.setopt(pycurl.READFUNCTION, FileReader(fd, self.file_size, filename, c).split_read)
         c.setopt(pycurl.URL, url)
         c.setopt(pycurl.HTTPHEADER, headers)
         c.setopt(pycurl.PUT, 1)

         # need to lookup more about getting the response mostly useless
         # probably a callback or something.....
         # Probably should put information in a file that is easily readable, maybe csv
         #print "Status Code: ", c.getinfo(pycurl.HTTP_CODE)
         #print "Total Transfer Time: ", c.getinfo(pycurl.TOTAL_TIME), " seconds."
         c.perform()
         c.close()
         fd.close()

    #===============================================================================
    # download_object downloads the file from url to the localspace filename. If something went wrong with
    #    writing the file it gets displayed to the command line.
    #      
    # It then prints the response code. Although I may change this to return a string of "what happened"
    # 
    # Example:def split_read():
    #     download_object(amplidata/dir1/dir2/file.txt, ~/downloaded.txt)
    #===============================================================================
     
    def download_object(self, url, filename):
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        try:
            fp = open(filename, "wb")
        except OSError:
            print "Something went terribly wrong."
            print OSError.message
            return
        c.setopt(pycurl.URL, url)
        out_header = open("outfile", "w+")
        c.setopt(pycurl.WRITEDATA, fp)
        c.setopt(pycurl.NOPROGRESS, 0)
        c.setopt(pycurl.PROGRESSFUNCTION, down_progress)
        c.setopt(pycurl.WRITEHEADER, out_header)
        c.perform()
        print "Status Code: ", c.getinfo(pycurl.HTTP_CODE)
        print "Total Transfer Time: ", c.getinfo(pycurl.TOTAL_TIME), " seconds."
        c.close()
        fp.close()
    

    def generate_statistics(self):
        fd = open("Transfer_Statistics", "a")
        while(self.statistics.empty() == False):
            fd.write(str(self.statistics.get()) +'\n')
        fd.write("~" + str(self.num_threads) +","+ str(self.bytes_transferred) + ","+ str(self.bytes_transferred/(self.end_time-self.start_time))+ "," + str(self.end_time-self.start_time) + '\n')
        fd.close()   
    
    #===============================================================================
    # The following functions are callbacks used by pycurl. They should never be called directly.
    #===============================================================================   
    def upload_thread(self, url, overwrite=False):
        while True:
            file = self.todo.get()
            try:
                result = self.send_object(file, url + '/' + file, overwrite)
                self.statistics.put(result)
            except:
                print "Something went wrong while sending the file (Can you contact the host?)"
            self.todo.task_done()


    def up_progress(self, download_t, download_d, upload_t, upload_d):
        percent = upload_d / (upload_t + 0.1) * 100
        if percent >= self.current:
            print int(percent), " % (", upload_d , "/", upload_t, ") complete"
            self.current += INCREMENT
        
    def down_progress(download_t, download_d, upload_t, upload_d):
        percent = download_d / (download_t + 0.1) * 100
        if percent >= self.current:
            print int(percent), " % (", download_d , "/", download_t, ") complete"
            self.current += INCREMENT
        
#===============================================================================
# Authenticated request should be created and called before we make any put and get requests
#     if using Authentication is requested by the user.
#     The first thing we do is send through our authentication request with our initial request (whatever that maybe)
#     The server responds with a 401 and an authentication header:
#        WWW-Authenticate: Digest
#        realm="Amplidata-AmpliStor/unknown-59b0f5a6e4709f566a5e8464153401ff76964cb9",
#        qop=auth, nonce="246a5bf426a77dc28c195ce0373a9d2c",
#        opaque="7e81b3abf2a84373b087cec73aca62c1"
#
#
#     Then we follow these steps http://en.wikipedia.org/wiki/Digest_access_authentication
#        Once authenticated, we can proceed with our initial request and every further request.
#        Each further request needs to recalculate the has with an increase in a synchronized variable of the count
#
#        Additionally, we need a listener that is looking for the STALE response. if that happens, we need
#        To do the authentication again. I'm not sure where this appears x: but it needs to signal all the threads to wait
#        until we are authenticated again
#
#===============================================================================
class AuthenticatedRequest:
    def __init__(self):
        pass

class ListHTMLParser():
    lines = []
    details = []
    #just so i have a reference to look at
    base_file = {"Type":None, "Name":None,"Size":None,"Last Modified":None}
    
    def __init__(self, filehandle):
        lines = filehandle.readlines()
        for line in lines:
            self.lines.append(line.strip())
    
    #===========================================================================
    # Searches through our lines and returns a list of lines leading with the passed tag or tags
    #===========================================================================
    def parse_lines_by(self, tag):
        result = []
        print tag
        for line in self.lines:
            if line.startswith(tag):
                result.append(line)
        return result
    
    def find_info(self, parsed_lines):
        #<tr><td valign="top"><img src="/icon/folder.ico" alt="[DIR]"></td><td><a href="/namespace/test/yp/">yp</a></td><td align="right">-</td><td align="right">-</td></tr>'
        #'<tr><td valign="top"><img src="/icon/file.ico" alt="[FILE]"></td><td><a href="/namespace/test/uh">uh</a></td><td align="right">Tue,&nbsp;26&nbsp;Feb&nbsp;2013&nbsp;23:49:20&nbsp;GMT</td><td align="right">8</td></tr>'
        for line in parsed_lines:
            #if we don't find this there is nothing useful on this line.
            start = line.find("[")
            info = {}
            #find filetype
            if start != -1:
                end = line.find("]",start)
                info['type'] = line[start+1, end]
                
                #find name, if name is dir add a / to the end of it
                start = line.find("<a href=\"")
                start = line.find(">", start) + 1
                end = line.find("<", start)
                info['name'] = line[start:end]
                
                #only do if it's not a dir
                if info['type'] != "DIR":
                    #length of line is 18
                    start = line.find("<td align=\"right\">")
                    start += 18
                    
                    end = line.find("<",start)
                    #this is a date string and still needs some formatting
                    info['Last Modified'] = line[start:end].replace("&nbsp;", " ")
                else:
                    info['name'] = info['name'] + '/'
                self.details.append(info)
            else:
                pass
            
    def print_lines(self):
        for line in self.lines:
            print line
    #cur_tag = None
    #def handle_starttag(self, tag, attrs):
     #   self.cur_tag = tag
      #  if (self.cur_tag == 'tr'):
      #      print attrs
    #def handle_data(self, data):
    #    if self.cur_tag == 'tr':
    #        print data
        
    
    
#eventually this needs to do something with threads lol.
#after that its to open it in 3 different places that are threaded and send all 3
class FileReader:
    def __init__(self, fp, filesize, filename, c):
        self.fp = fp
        self.filesize = filesize
        #number of threads
        self.block = filesize / 10
        self.filename = filename
        self.c = c
    def split_read(self, fp):
        print "splitting"
        tracker = Queue.Queue()
        for i in range(0, 10):
            tracker.put(i * self.file_size / 10)
            t = threading.Thread(target=self.large_file_upload, args=[])
                #python handles cleanup when program exits
            t.daemon = True
            t.start()
            print "spawn thread:", i
        tracker.join() #block until finished
        print "Finished"
        
    def large_file_upload(self):
        #gets starting point   
        task = tracker.get()
        print "My task is write from:", str(task), "to:", str(task)*self.block
        tracker.task_done()
        returnatlest
        if bytes_read == block:
            return ''
        fd = open(filename, 'rb')
        fd.seek(task)
        readed = fd.read(self.block)
        fd.close()
        return readed

if __name__ == "__main__":
    main()



