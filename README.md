=======
AmpliShare
==========

Instructions for installation:
-----------------------------
- Unpack the src folder "somewhere"
- Create a symbolic link to AmpliTest_2.6 in /usr/bin with the command name of your choice.
- Run the application with flag -h for a help message.

File transfer statistics are appended to the file Transfer_Statistics in the format as follows:
For each transfer it looks like
filename1,size of file,time to send,HTTP status
filename2,size of file, time to send,HTTP status
...
filenameN,size of file,time to send,HTTP status
~num_threads,total_sent,total_time,Bytes/second


Sending and recieving files from amplidata
b236512618d4537c4f00c761022f8aefb74a9a06
